CREATE TABLE users (
	id VARCHAR(25) NOT NULL, 
	login VARCHAR(25), 
	display_name VARCHAR(250), 
	type VARCHAR(25), 
	broadcaster_type VARCHAR(25), 
	description VARCHAR(300), 
	profile_image_url VARCHAR(300), 
	offline_image_url VARCHAR(300), 
	view_count INTEGER, 
	last_check TIMESTAMP WITHOUT TIME ZONE, 
	PRIMARY KEY (id)
);
CREATE TABLE views_follows (
	query_tstamp TIMESTAMP WITHOUT TIME ZONE, 
	user_id VARCHAR(25), 
	view_count INTEGER, 
	follows_count INTEGER
);
CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
SELECT create_hypertable('views_follows', 'query_tstamp');
