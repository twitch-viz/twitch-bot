"""Useful stuff stays here,
until it grows enough to need
its own file/module.
"""

from config import USERNAME, PSQL_PASSWD, PSQL_HOST, DB_NAME
from sqlalchemy import create_engine, MetaData

def render_passwd(code):
    """Poor man's decryption of codes"""
    passwd = ''.join([x[0] for x in code.split(" ")])
    return passwd


def db_engine_setup():
    """Setup DB engine
    The returned Engine object can be used to established
    a connection through a context manager using
    the .begin() method.

    :return: SQLAlchemy Engine instance
    """
    psql_password = render_passwd(PSQL_PASSWD)
    engine = create_engine('postgresql+psycopg2://{}:{}@{}/{}'.format(
        USERNAME, psql_password, PSQL_HOST, DB_NAME
    ), echo=True)
    return engine


def db_reflect_metadata(engine):
    """Reflect the existing database metadata
    The returned object contains all information
    on database tables and columns.

    :param engine: SQLAlchemy Engine instance
    :return: MetaData instance
    """
    metadata = MetaData(engine)
    metadata.reflect(bind=engine)
    return metadata


if __name__ == '__main__':

    engine = db_engine_setup()
    metadata = db_reflect_metadata(engine)
    print(metadata.tables.keys())
