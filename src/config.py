import os
from datetime import timedelta

# DB CONFIG
USERNAME = os.environ['TDB_USERNAME']
# UNIX_PASSWD = os.environ['TDB_UNIX_PASSWD']
PSQL_PASSWD = os.environ['PSQL_PASSWD']
PSQL_HOST = os.environ['PSQL_HOST']
DB_NAME = USERNAME

# TWITCH CREDENTIALS
TWITCH_CLIENT_ID = os.environ['TWITCH_CLIENT_ID']
TWITCH_CLIENT_SECRET = os.environ['TWITCH_CLIENT_SECRET']
TWITCH_REDIRECT_URI = os.environ['TWITCH_REDIRECT_URI']
TWITCH_ACCESS_TOKEN = os.environ['TWITCH_ACCESS_TOKEN']
TWITCH_AUTH_URL = os.environ['TWITCH_AUTH_URL']
TWITCH_BASE_URL = os.environ['TWITCH_BASE_URL']
TWITCH_STREAMS_URL = TWITCH_BASE_URL + 'streams/'
TWITCH_USERS_URL = TWITCH_BASE_URL + 'users/'
TWITCH_FOLLOWS_URL = TWITCH_USERS_URL + 'follows/'


# OTHER SETTINGS
# How often we want to record the data?
BOT_FREQUENCY = timedelta(hours=1)
# When checking for time equality, how precise we want to be? (In seconds)
TIMEDELTA_PRECISION = 60
# Timeout for requests
REQUESTS_TIMEOUT = 20
# How many users to track
N_TOP_USERS_VIEWS = 2000


if __name__ == '__main__':
    print(USERNAME)


