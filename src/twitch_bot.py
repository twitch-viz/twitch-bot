"""
Docstring goes here.
"""
import random
from datetime import datetime, timedelta
from itertools import zip_longest
import json
import logging
from requests.adapters import HTTPAdapter
from requests.exceptions import HTTPError, ConnectionError, Timeout
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import BackendApplicationClient
from sqlalchemy import select, insert
from time import sleep
from urllib3.util.retry import Retry

from config import (
    BOT_FREQUENCY, TIMEDELTA_PRECISION, REQUESTS_TIMEOUT,
    TWITCH_CLIENT_ID, TWITCH_CLIENT_SECRET,
    TWITCH_ACCESS_TOKEN, TWITCH_AUTH_URL,
    TWITCH_STREAMS_URL, TWITCH_USERS_URL, TWITCH_FOLLOWS_URL,
    N_TOP_USERS_VIEWS,
)
from utils import db_engine_setup, db_reflect_metadata

# These are for debugging
# BOT_FREQUENCY = timedelta(seconds=900)
# TIMEDELTA_PRECISION = 60


logger = logging.getLogger(__name__)


class NetworkError(RuntimeError):
    """Error produced by the retryer when it gives up"""
    pass


def retryer(func):
    """Decorator retry function for requests

    :param func:
    :return:
    """
    retry_on_exceptions = (
        Timeout,
        ConnectionError,
        HTTPError,
    )
    max_retries = 40
    timeout = 60

    def inner(*args, **kwargs):
        """
        Retry as much as needed, then throw an exception

        Error 401 is treated separately so that
        the application can handle it when needed.
        """
        try:
            for i in range(max_retries):
                try:
                    result = func(*args, **kwargs)
                except retry_on_exceptions:
                    sleep(timeout)
                    continue
                else:
                    return result
        # After many tries, give up
        # This error will probably come from
        # expired authentication, so we bubble it up
        # in order for the app to renew the access token
        except HTTPError as e:
            if e.response.status_code == 401:
                raise
            else:
                logger.error("Failed to succeed after many retries")
                raise NetworkError
        else:
            logger.error("Failed to succeed after many retries")
            raise NetworkError

    return inner


def is_it_time(last_checkpoint):
    """Did the requested timedelta pass since last_checkpoint?

    This closure uses the config variables TIMEDELTA_PRECISION and BOT_FREQUENCY.

    :param last_checkpoint: datetime
    :return: bool
    """
    delta_t = (
            last_checkpoint - datetime.utcnow()
    ) + BOT_FREQUENCY
    # logger.debug(
    #     "Time remaining to next checkpoint: {}".format(delta_t)
    # )
    return delta_t.seconds <= TIMEDELTA_PRECISION or delta_t.days < 0


def wait_until_next_cycle(last_checkpoint):
    """Keep sleeping until a full period has passed.

    This closure uses the config variable TIMEDELTA_PRECISION.

    :param last_checkpoint:
    :return:
    """

    sleep(TIMEDELTA_PRECISION)  # Ensures we slip in the next time slot

    # logger.debug("Sleeping {} secs...".format(TIMEDELTA_PRECISION))
    logger.debug("Waiting for next cycle until {}...".format(
        last_checkpoint + BOT_FREQUENCY
    ))
    while not is_it_time(last_checkpoint):
        sleep(TIMEDELTA_PRECISION)

    return datetime.utcnow()


def db_get_users_views(users_table, engine):
    """Fetch all user ids and respective view count from DB

    :param users_table: A SQLAlchemy Table object
    :param engine: A SQLAlchemy Engine object
    :return:
    user_views: dict
    """
    with engine.begin() as conn:
        result = conn.execute(
            select([users_table.c.id, users_table.c.view_count])
        )
        users_views = {x.id: x.view_count for x in result}
        # user_id_set = {x.id for x in result}
    return users_views


def db_get_top_users_views(users_table, engine, top_limit=10):
    """Fetch top user ids and respective view count from DB,
    ordered by view count

    :param top_limit: int
    :param users_table: A SQLAlchemy Table object
    :param engine: A SQLAlchemy Engine object
    :return:
    user_views: dict
    """
    with engine.begin() as conn:
        result = conn.execute(
            select(
                [users_table.c.id, users_table.c.view_count]
            ).order_by(
                users_table.c.view_count.desc()
            ).limit(top_limit)
        )
        users_views = {x.id: x.view_count for x in result}
        # user_id_set = {x.id for x in result}
    return users_views


def get_twitch_credentials():
    """Collect twitch credentials

    :return:
    client_id: str
    token: dict
    """
    client_id = TWITCH_CLIENT_ID
    token = dict(access_token=TWITCH_ACCESS_TOKEN)
    return client_id, token


def oauth_header(token):
    """Construct session header with access token

    This follows the New Twitch API reference.

    :param token: str
    :return: str
    """
    return {
        "Authorization": "Bearer {}".format(
            token
        )
    }


def test_api_valid(oauth_session, test_url):
    """Test API call and check for errors

    :param oauth_session: requests_oauthlib session object
    :param test_url: str
    :return: None
    """
    test_url = TWITCH_STREAMS_URL
    tresp = oauth_session.get(
        test_url,
        params=dict(first=1),
        timeout=REQUESTS_TIMEOUT,
    )
    response_check_errors(tresp)


def wait_api_limits(response):
    """Check if ratelimit reached and wait until expired

    :param response: response object
    :return: None
    """
    hds = response.headers
    #     break
    try:
        if int(hds['Ratelimit-Remaining']) == 0:
            delay = int(
                hds['Ratelimit-Reset']
            ) - datetime.timestamp(datetime.now())
            logger.debug("Ratelimit reached. Sleeping {} secs...".format(delay))
            sleep(delay+1)
    except Exception as e:
        logger.debug(e)
        raise


def grouper(iterable, n, fillvalue=None):
    "Collect data into fixed-length chunks or blocks"
    # grouper('ABCDEFG', 3, 'x') --> ABC DEF Gxx"
    args = [iter(iterable)] * n
    return zip_longest(*args, fillvalue=fillvalue)


def response_check_errors(response):
    """Handle response exceptions

    :param response: requests response object
    :return: None
    """
    try:
        response.raise_for_status()
    except HTTPError as e:
        logger.error(e)
        logger.debug("Response headers:")
        logger.debug(json.dumps(dict(response.headers), indent=4))
        logger.warning("Request failed. Deal with it.")
        raise


@retryer
def fetch_stream_ids(oauth):
    """Fetch user ids from active twitch streams

    Under 50 views the function stops fetching pages.

    :param oauth: requests_oauthlib session object
    :return: set
    """
    # Setup pagination
    page = None
    new_user_ids = set()
    # Loop through pages
    min_views = 1000
    while min_views >= 50:  # TODO: This arbitrary number shouldn't be hard-coded (config?)

        oauth.params = dict(first=100)
        # Update pagination
        if page:
            oauth.params.update(
                dict(after=page)
            )

        response = oauth.get(
            TWITCH_STREAMS_URL,
            timeout=REQUESTS_TIMEOUT,
        )
        response_check_errors(response)

        resp_json = response.json()
        page = resp_json['pagination']['cursor']

        for d in resp_json['data']:
            new_user_ids.add(d.get('user_id'))
        logger.debug("IDs found: {}".format(len(new_user_ids)))

        min_views = min([d['viewer_count'] for d in resp_json['data']])
        logger.debug("Min viewer count: {}".format(min_views))

        wait_api_limits(response)

    return new_user_ids


@retryer
def fetch_following_ids(user_id, oauth):
    """Fetch list of users followed by a given user

    :param user_id:
    :param oauth:
    :return: set
    """
    # Setup pagination
    page = None
    new_user_ids = set()
    # Loop through pages
    done = False
    while not done:

        oauth.params = dict(first=100, from_id=user_id)
        # Update pagination
        if page:
            oauth.params.update(
                dict(after=page)
            )

        response = oauth.get(
            TWITCH_FOLLOWS_URL,
            timeout=REQUESTS_TIMEOUT,
        )
        response_check_errors(response)

        resp_json = response.json()
        logger.debug("Total following found: {}".format(resp_json['total']))
        for d in resp_json['data']:
            new_user_ids.add(d.get('to_id'))

        page = resp_json['pagination']['cursor']
        done = (not resp_json['data']) or (resp_json['total'] <= 100)
        wait_api_limits(response)

    logger.debug("IDs found: {}".format(len(new_user_ids)))

    return new_user_ids


def fetch_users_following(new_user_ids, oauth):
    """Fetch users followed by a given set of users

    :param new_user_ids: set
    :param oauth: requests_oauthlib session object
    :return: set
    """
    new_users = set()
    for user_id in new_user_ids:
        logger.debug("Fetching following from user {}".format(user_id))
        following_ids = fetch_following_ids(user_id, oauth)
        new_users.update(following_ids)
        logger.debug("Found {} follows so far.".format(len(new_users)))

    return new_users


def db_insert_users(db_engine, new_users, users_table):
    """Insert list of users to the users table in the DB

    :param db_engine: SQLAlchemy engine object
    :param new_users: iterable
    :param users_table: SQLAlchemy table object
    :return: None
    """
    logger.debug("Inserting {} users to DB...".format(len(new_users)))
    with db_engine.begin() as conn:
        try:
            result = conn.execute(
                insert(users_table, new_users)
            )
        except Exception as e:
            logger.debug("Something went wrong inserting users:")
            logger.error(e)
            raise


@retryer
def fetch_users(user_ids, oauth):
    """Fetch user API data for the given ids

    :param user_ids:
    :param oauth:
    :return: list of dicts
    """
    new_users = []
    for user_ids in grouper(user_ids, 100):  # TODO: set the max n users (100) somewhere else
        params = {
            'id': [x for x in user_ids if x is not None]
        }
        response_users = oauth.get(
            TWITCH_USERS_URL,
            params=params,
            timeout=REQUESTS_TIMEOUT,
        )
        response_check_errors(response_users)
        resp_json = response_users.json()
        user_data = resp_json['data']

        new_users.extend(user_data)

        wait_api_limits(response_users)
    return new_users


def save_token(token):
    with open("access_token.json", 'w') as f:
        json.dump(token, f, ensure_ascii=False)


def setup_oauth_session(client_id):
    """Setup the session object with a new token

    This closure takes care of:
    - Authorization
    - Retries

    :return: None
    """
    client = BackendApplicationClient(
        client_id=client_id
    )
    oauth = OAuth2Session(client=client)
    # , token=token)
    # oauth = renew_session(client_id, oauth)
    token = oauth.fetch_token(
        TWITCH_AUTH_URL,
        client_id=client_id,
        client_secret=TWITCH_CLIENT_SECRET,
    )
    save_token(token)
    oauth.headers.update(oauth_header(token))
    retry = Retry(total=99, backoff_factor=0.1)
    adapter = HTTPAdapter(max_retries=retry)
    oauth.mount('https://', adapter)
    return oauth


def get_top_views_id(users_views, first=100):
    """Get ids of top X users ordered by view count

    :param users_views: dict
    :param first: int (Optional)
    :return: list
    """
    top_ids = list(
        map(
            lambda x: x[0],
            sorted(users_views.items(), key=lambda x: x[1], reverse=True)
        )
    )[:first]
    return top_ids


@retryer
def fetch_users_follows(users_ids, oauth):
    user_follows = {}
    for i, user_id in enumerate(users_ids):
        logger.debug("Processing user {} of {}".format(i, len(users_ids)))
        logger.debug("Fetching total followers for user {}".format(user_id))
        oauth.params = dict(
            to_id=user_id,
            first=1,
        )
        response = oauth.get(
            TWITCH_FOLLOWS_URL,
            timeout=REQUESTS_TIMEOUT,
        )
        response_check_errors(response)
        user_follows[user_id] = response.json()['total']

        wait_api_limits(response)

    return user_follows


def fetch_n_views_follows(top_users_ids, oauth):
    """Fetch views and followers for the given user ids


    :param top_users_ids: set
    :param oauth: requests_authlib session object
    :return: list of dicts
    This is a json-like list, almost ready to be ingested
    by the DB
    """
    users_follows = fetch_users_follows(top_users_ids, oauth)
    users_data = fetch_users(top_users_ids, oauth)
    users_views = {
        d['id']: d['view_count'] for d in users_data
    }
    views_follows = [
        dict(
            user_id=_uid,
            view_count=users_views.get(_uid),
            follows_count=users_follows.get(_uid),
        ) for _uid in top_users_ids
    ]
    return views_follows


def db_insert_views_follows(db_engine, users_views_followers, views_follows_table):
    # logger.debug("Inserting {} users to DB...".format(len(new_users)))
    with db_engine.begin() as conn:
        try:
            result = conn.execute(
                insert(views_follows_table, users_views_followers)
            )
        except Exception as e:
            logger.debug("Something went wrong inserting users:")
            logger.error(e)
            raise


def main():

    logger.debug("DB Setup...")
    db_engine = db_engine_setup()
    metadata = db_reflect_metadata(db_engine)
    logger.debug(metadata.tables.keys())

    users_table = metadata.tables['users']
    views_follows_table = metadata.tables['views_follows']

    # Setup initial user_id set
    logger.debug("Fetch users set from DB...")
    users_views = db_get_users_views(users_table, db_engine)
    user_id_set = {x for x in users_views.keys()}
    logger.debug("Found {} unique users in the DB.".format(len(user_id_set)))

    logger.debug("Setting up API session...")
    client_id, token = get_twitch_credentials()
    oauth = setup_oauth_session(client_id)

    logger.debug("Testing API credentials...")
    try:
        test_api_valid(oauth, TWITCH_STREAMS_URL)
    except Exception as e:
        logger.error(e)

    last_checkpoint = datetime.utcnow()
    while True:
        try:
            ### CHECK CURRENT STREAMS ###
            logger.debug("Fetch Twitch streams...")
            # TODO: the 401 error is only handled here,
            # TODO: if it happens anywhere else after this point, the application will crash.
            try:
                new_user_ids = fetch_stream_ids(oauth)
            except HTTPError as e:
                logger.warning(e)
                # This assumes http error will only come from expired authentication
                logger.info("Will attempt to renew credentials and retry")
                oauth = setup_oauth_session(client_id)
                continue

            # Exclude users already known
            new_user_ids = new_user_ids.difference(user_id_set)
            ### QUERY USERS FOLLOWS (FROM) ###
            logger.debug("Querying users following from top channels...")
            # top_ids = get_top_views_id(users_views)
            new_user_ids.update(
                fetch_users_following(
                    random.sample(
                        new_user_ids, min(len(new_user_ids), 50)
                    ),
                    oauth
                )
            )
            # TODO: Update known users instead of ignoring them
            new_user_ids = new_user_ids.difference(user_id_set)
            logger.debug("Actually new users: {}".format(len(new_user_ids)))

            ### QUERY NEW USERS FOR VIEWS ###
            if new_user_ids:
                new_users = fetch_users(new_user_ids, oauth)
                for u in new_users:
                    u['last_check'] = last_checkpoint
                ### INSERT NEW USERS TO DB ###
                db_insert_users(db_engine, new_users, users_table)
                user_id_set = user_id_set.union(new_user_ids)

            ### GET TOP USERS FOR VIEW_COUNT ###
            top_users_views = db_get_top_users_views(users_table, db_engine, top_limit=N_TOP_USERS_VIEWS)
            users_views_followers = fetch_n_views_follows(top_users_views.keys(), oauth)
            for d in users_views_followers:
                d['query_tstamp'] = last_checkpoint

            db_insert_views_follows(db_engine, users_views_followers, views_follows_table)

            ### FINAL HOUSEKEEPING ###
            logger.debug("Cycle end reached.")
            last_checkpoint = wait_until_next_cycle(last_checkpoint)

        except Exception as e:
            logger.critical(e)
            logger.critical("Will just try to restart from the beginning")
            continue


if __name__ == '__main__':

    logging.basicConfig(
        format='%(asctime)s - %(levelname)s:%(message)s',
        level=logging.INFO,
    )
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # logger.setLevel(logger.debug)
    fh = logging.FileHandler('logger.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    main()
