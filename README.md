# Twitch crawler


## Bugs and fixes

### Timescaledb setting after Ubuntu update

While logged on the machine
(use remote logging otherwise):

```
sudo -i -u postgres psql -X
```
```
\c twitchbot
```
```
ALTER EXTENSION timescaledb UPDATE;
```
Then ```\dx``` should return something like
(depending on versions and extensions)
```
                                      List of installed extensions
    Name     | Version |   Schema   |                            Description                            
-------------+---------+------------+-------------------------------------------------------------------
 plpgsql     | 1.0     | pg_catalog | PL/pgSQL procedural language
 timescaledb | 0.9.2   | public     | Enables scalable inserts and complex queries for time-series data
(2 rows)
```